from random import randint
from node import Node


class Graph:
    def __init__(self, sizex: int,sizey:int):
        self.sizex = sizex
        self.sizey = sizey
        self.table = [[0 for x in range(sizex)] for y in range(sizey)]  # create matrix size x size
        self.tuning()  # fill the matrix

    def __str__(self):
        line = 'Weights:\n'
        for x in range(0, self.sizex):
            line = line + str(x) + '\t'
            for y in range(0, self.sizey):
                line = line + str(self.table[x][y].weight) + '\t'
            line = line + '\n'
        line = line + 'g:\n'
        for x in range(0, self.sizex):
            line = line + str(x) + '\t'
            for y in range(0, self.sizey):
                line = line + str(self.table[x][y].g) + '\t'
            line = line + '\n'
        return line
    
    def getNeighbourList(self, x, y):
        neighbour_list = []
        if x > 0:
            neighbour_list.append(self.table[x - 1][y])
        if x < (self.sizex - 1):
            neighbour_list.append(self.table[x + 1][y])
        if y > 0:
            neighbour_list.append(self.table[x][y - 1])
        if y < (self.sizey - 1):
            neighbour_list.append(self.table[x][y + 1])
        print(neighbour_list)
        return neighbour_list
    
    def get_direction(self, current_node):
        if current_node.x > self.x and current_node.y == self.y:
            return 3
        elif current_node.x < self.x and current_node.y == self.y:
            return 1
        elif current_node.x == self.x and current_node.y > self.y:
            return 2
        elif current_node.x == self.x and current_node.y < self.y:
            return 0

    def tuning(self):
        for x in range(0, self.sizex):
            for y in range(0, self.sizey):
                self.table[x][y] = Node(x, y, randint(1, 101), [])
        for x in range(0, self.sizex):
            for y in range(0, self.sizey):
                self.table[x][y].update_neighbour_list(self.getNeighbourList(x, y))
        
