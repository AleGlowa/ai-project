import numpy as np
import tensorflow as tf
import cv2                                                      # for loading images
from useful_utils import FaceAligner
import dlib                                                     # for loading pre-trained facial landmarks predictor
import matplotlib.pyplot as plt                                 # for displaying images
import matplotlib.image as mpimg                                # for displaying images

classes = {0: 'no smile', 1: 'low intensity smile', 2: 'high intensity smile'}
no_class = 3

def build_model(features, labels, mode):
    # 1 CONV
    conv1 = tf.layers.conv2d(features, 32, 5, padding='same', activation=tf.nn.relu, name='conv1')
    pool1 = tf.layers.max_pooling2d(conv1, 2, 2, name='pool1')
    
    # DENSE
    reshape = tf.reshape(pool1, [-1, 63 * 47 * 32])
    dense1 = tf.layers.dense(reshape, 400, activation=tf.nn.relu, name='dense1')
    
    # LOGITS
    logits = tf.layers.dense(dense1, no_class, name='logits')

    # PREDICTIONS
    predicted_classes = tf.argmax(input=logits, axis=1)
    predictions = {
        'classes': predicted_classes[:, tf.newaxis],
        'probabilities': tf.nn.softmax(logits, name='softmax')
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # LOSS FUNCTION
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    
    # IF mode==TRAIN run train_op and return it
    if mode == tf.estimator.ModeKeys.TRAIN:
        # LEARNING RATE
        learning_rate = tf.train.exponential_decay(0.01, tf.train.get_global_step(), 50, 0.96, staircase=True)
        optimizer = tf.train.MomentumOptimizer(learning_rate, 0.9)
        train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss)

def parse_function(image_path):
    image_string = tf.read_file(image_path)
    if image_path[-3:] == 'jpg' or image_path[-3:] == 'jpeg':
        image = tf.image.decode_jpeg(image_string, 1)
    if image_path[-3:] == 'png':
        image = tf.image.decode_png(image_string, 1)
    image = tf.image.convert_image_dtype(image, tf.float32)

    scale = 1 / 3
    new_height, new_width = int(378 * scale), int(285 * scale)
    image = tf.image.resize_images(image, [new_height, new_width])

    return image

def preprocess_new_example(image_path, person_name):

    file_name = image_path.split('/')[-1]
    file_name, extension = file_name.split('.')[0:-1], file_name.split('.')[-1]
    file_name = '.'.join(file_name)
    preprocessed_path = 'new_examples/' + person_name + '/' + file_name + '_preprocessed.' + extension
    image = cv2.imread(image_path)
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
    faceAligner = FaceAligner(predictor=predictor)
    rects = detector(gray_image, 2)
    for rect in rects:
        aligned = faceAligner.align(gray_image, rect=rect)
    cv2.imwrite(preprocessed_path, aligned)
    new_image = parse_function(preprocessed_path)
    new_image = new_image[tf.newaxis, :]

    return new_image, preprocessed_path

def recognize_smile(image_path, person_name):
    '''
        params: image_path
                    Path to image for predicting whether it contains smile or not
                person_name
                    Catalog in which save preprocessed image
    '''

    preprocessed_image, preprocessed_path = preprocess_new_example(image_path, person_name)

    cust_predict_new_example_input_fn = tf.estimator.inputs.numpy_input_fn(
        x=tf.Session().run(preprocessed_image),
        batch_size=1,
        shuffle=False
    )

    predict_result = smile_classifier.predict(input_fn=cust_predict_new_example_input_fn)
    for pred_dict in predict_result:
        class_id = pred_dict['classes'][0]
        probability = pred_dict['probabilities'][class_id]
        img = mpimg.imread(preprocessed_path)
        plt.title('Prediction is {} ({:.2f}%)'.format(classes[class_id], 100 * probability))
        plt.imshow(img)
        plt.xticks([])
        plt.yticks([])
        plt.show()

    return classes[class_id]

smile_classifier = tf.estimator.Estimator(model_fn=build_model, model_dir='models/final_model')

# recognize smile
#_ = recognize_smile('new_examples/person/person8.jpg', 'person')