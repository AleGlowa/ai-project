import random
import numpy
optimal=[
        1,2,3,4,5,6,7,8,9,10,11,12]
DNA_SIZE=12
dna_check_value=[]
weight_matrix=[[0,20,17,14,14,11,8,20,20,2,14,11,8],
               [20,0,3,6,6,9,12,12,15,27,18,21,30],
               [17,3,0,3,9,6,9,15,12,15,21,18,21],
               [14,6,3,0,12,9,6,18,15,12,24,21,18],
               [14,6,9,12,0,3,6,6,9,12,12,15,18],
               [11,6,6,6,3,0,3,6,6,6,15,12,15],
               [8,12,9,3,3,0,12,9,6,12,9,12,12],
               [20,12,15,18,6,9,12,0,3,6,6,9,12],
               [20,15,12,15,9,6,9,3,0,3,9,6,9],
               [2,18,15,12,12,9,6,6,3,0,12,9,6],
               [14,18,21,24,12,15,18,6,9,12,0,3,6],
               [11,21,18,21,15,12,15,9,6,9,3,0,3],
               [8,30,21,18,18,15,12,12,9,6,6,3,0]]
print(optimal[2])
POP_SIZE    = 20
GENERATIONS = 100
#def BestUnitChoice(units):
        #max=units[0]
        #for one in units:
                #currentFit=
def weighted_choice(items):
  """
  Losowy przedmiot z listy, lista tupli (item,weight)
  """
  weight_total = sum((item[1] for item in items))
  n = random.uniform(0, weight_total)
  for item, weight in items:
    if n < weight:
      return item
    n = n - weight
  return item

def random_node():
        x=random.sample(optimal,12)
        dna_check_value=x
        print(dna_check_value)
        return dna_check_value
def random_population():

  pop = []
  dna=[0,0,0,0,0,0,0,0,0,0,0,0]
  for i in range(POP_SIZE):
   dna=random_node()
   pop.append(dna)
  return pop
def fitness(dna):

  fitness = 0
  fitness=fitness+weight_matrix[0][dna[0]]
  fitness=fitness+weight_matrix[0][dna[-1]]
  
  for c in range(1,9,1):
    fitness = fitness + weight_matrix[dna[c]][dna[c+1]]
  return fitness

def mutate(dna):

  dna_out = [0,0,0,0,0,0,0,0,0,0,0,0]
  mutation_chance = 100
  if int(random.random()*mutation_chance) == 1:
      dna_out=dna
      print(dna_out)
      x=dna_out[0]
      y=dna_out[1]
      z=dna_out[3]
      u=dna_out[7]
      dna_out[0]=y
      dna_out[1]=x
      dna_out[3]=u
      dna_out[7]=z
  else:
      dna_out=dna
  return dna_out

def crossover(dna1, dna2):
  """
  Potnij dna1 i dna2 w dwie czesci na losowym indeksie 
  i zlacz je. Zachowujac podlisty do momentu krzyzowania ale ich konce sa zmieniane.
  """
  dna_pot_1=[]
  dna_pot_2=[]
  dna_pot_1.clear()
  dna_pot_2.clear()
  for a in range(0,6,1):
        dna_pot_1.append(dna1[a])
        dna_pot_2.append(dna2[a])
  for i in range(0,12,1):
          if dna1[i] not in dna_pot_2:
                  dna_pot_2.append(dna1[i])
  for j in range(0,12,1):
          if dna2[j] not in dna_pot_1:               
                  dna_pot_1.append(dna2[j])
  return (dna_pot_1, dna_pot_2)

def genetic_algorithm():
        population = random_population()
        print(population)
        for generation in range(GENERATIONS):
                print ("Generation %s... Random sample: '%s'" % (generation, population[0]))
    
                weighted_population = []


                for individual in population:
                        fitness_val = fitness(individual)
      # Generowanie (individual,fitness) pary, majac
      # na uwadze aby przez przypadek nie dzielila sie przez 0.
                        if fitness_val == 0:
                                pair = (individual, 1.0)
                        else:
                                pair = (individual, 1.0/fitness_val)

                weighted_population.append(pair)
                population = []

   #Wybranie dwoch losowych obiektow z populacji bazujac na ich prawdopodobienstwie
                for _ in range(int(POP_SIZE)):
      # Selection
                        ind1 = weighted_choice(weighted_population)
                        ind2 = weighted_choice(weighted_population)

      # Crossover
                        ind1, ind2 = crossover(ind1, ind2)

      # Mutate and add back into the population.
                        population.append(mutate(ind1))
                        population.append(mutate(ind2))


                        fittest = population[0]
                        minimum_fitness = fitness(population[0])

                        for individual in population:
                                ind_fitness = fitness(individual)
                                if ind_fitness <= minimum_fitness:
                                        fittest = individual
                                        minimum_fitness = ind_fitness

        print("Optimal route: ", fittest," Route cost: ",fitness_val)
        return fittest