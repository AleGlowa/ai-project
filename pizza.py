# size is added in cm's
# pizza with all 0s is the Error Pizza and it could not be ordered! (0 size ;P)
# for now this class is unused, if u want to use it -it is pretty declared though ;)


class Pizza:
    def __init__(self, size: int,
                 ham: int, beacon: int, chicken: int,
                 cheese: int, gorgonzola: int, brie: int, pecorino: int,
                 tomatoes: int, pineapple: int, thyme: int,
                 basil: int, oregano: int,
                 pepper: int, jalapeno: int):
        self.size = size
        self.isVegetarian = self.tune([ham, beacon, chicken])
        self.isLightDigestible = self.tune([cheese, gorgonzola, brie, pecorino])
        self.isAllergicFriendly = self.tune([self.isLightDigestible, tomatoes, pineapple, thyme])
        # meats --> isVegetarian = False
        self.ham = ham
        self.beacon = beacon
        self.chicken = chicken
        # cheeses --> isLightDigestible = False and isAlergicFriendly = False
        self.cheese = cheese
        self.gorgonzola = gorgonzola
        self.brie = brie
        self.pecorino = pecorino  # owcze mleko, podobno bardzo dobry do pizzy ~ zasluga Adam Borys
        # strong allergens --> isAllergicFriendly = False
        self.tomatoes = tomatoes
        self.pineapple = pineapple
        self.thyme = thyme
        # seasonings (w-out allergic ones)
        self.basil = basil
        self.oregano = oregano
        # vegetables (w-out allergic ones)
        self.pepper = pepper
        self.jalapeno = jalapeno
        self.__dict__ = {
            "size": self.size,
            "isVegetarian": self.isVegetarian,
            "isLightDigestible": self.isLightDigestible,
            "isAllergicFriendly": self.isAllergicFriendly,
            "ham": self.ham,
            "beacon": self.beacon,
            "chicken": self.chicken,
            "cheese": self.cheese,
            "gorgonzola": self.gorgonzola,
            "brie": self.brie,
            "pecorino": self.pecorino,
            "tomatoes": self.tomatoes,
            "pineapple": self.pineapple,
            "thyme": self.thyme,
            "basil": self.basil,
            "oregano": self.oregano,
            "pepper": self.pepper,
            "jalapeno": self.jalapeno
        }

    def __str__(self):
        return ('pizza.Pizza(' +
            str(self.size) + ', ' +
            str(self.ham) + ', ' +
            str(self.beacon) + ', ' +
            str(self.chicken) + ', ' +
            str(self.cheese) + ', ' +
            str(self.gorgonzola) + ', ' +
            str(self.brie) + ', ' +
            str(self.pecorino) + ', ' +
            str(self.tomatoes) + ', ' +
            str(self.pineapple) + ', ' +
            str(self.thyme) + ', ' +
            str(self.basil) + ', ' +
            str(self.oregano) + ', ' +
            str(self.pepper) + ', ' +
            str(self.jalapeno) + '),')

    @staticmethod
    def tune(ingredient_list):
        response = False
        ingredient_id = 0
        while not response and ingredient_id < len(ingredient_list):
            if ingredient_list[ingredient_id] == 0:
                response = True
            ingredient_id += 1
        return response
    
    def getAttribute(self, string):
        print('wufbwuefubwe ', string)
        return getattr(self, string)

    @staticmethod
    def get_attributes():
        return [
            'size',
            'isVegetarian',
            'isLightDigestible',
            'isAllergicFriendly',
            'ham',
            'beacon',
            'chicken',
            'cheese',
            'gorgonzola',
            'brie',
            'pecorino',
            'tomatoes',
            'pineapple',
            'thyme',
            'basil',
            'oregano',
            'pepper',
            'jalapeno'
        ]