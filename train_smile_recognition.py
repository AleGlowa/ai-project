import numpy as np
import tensorflow as tf
from useful_utils import get_data_set, FaceAligner, rect_to_bb
from sklearn.model_selection import train_test_split            # for splitting training/validation/test sets
import logging                                                  # for debugging

dir_X = 'preprocessed_data/frames/'
dir_Y = 'data/labels/'

epochs = 50
batch_size = 256
no_class = 3
classes = {0: 'no smile', 1: 'low intensity smile', 2: 'high intensity smile'}
data_size = 130788
train_shuffle_buffer = 83704

logging.getLogger().setLevel(logging.INFO)

def build_model(features, labels, mode):
    # 1 CONV
    conv1 = tf.layers.conv2d(features, 32, 5, padding='same', activation=tf.nn.relu, name='conv1')
    pool1 = tf.layers.max_pooling2d(conv1, 2, 2, name='pool1')
    
    # DENSE
    reshape = tf.reshape(pool1, [-1, 63 * 47 * 32])
    dense1 = tf.layers.dense(reshape, 400, activation=tf.nn.relu, name='dense1')
    
    # LOGITS
    logits = tf.layers.dense(dense1, no_class, name='logits')

    # PREDICTIONS
    predicted_classes = tf.argmax(input=logits, axis=1)
    predictions = {
        'classes': predicted_classes[:, tf.newaxis],
        'probabilities': tf.nn.softmax(logits, name='softmax')
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # LOSS FUNCTION
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    
    # IF mode==TRAIN run train_op and return it
    if mode == tf.estimator.ModeKeys.TRAIN:
        # LEARNING RATE
        learning_rate = tf.train.exponential_decay(0.01, tf.train.get_global_step(), 50, 0.96, staircase=True)
        optimizer = tf.train.MomentumOptimizer(learning_rate, 0.9)
        train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss)

def train_input_fn(features, labels, batch_size):
    
    dx_train = tf.data.Dataset.from_tensor_slices(features)
    dy_train = tf.data.Dataset.from_tensor_slices(labels)
    train_dataset = tf.data.Dataset.zip((dx_train, dy_train))

    def parse_function(file_name, label):
        image_string = tf.read_file(file_name)
        image = tf.image.decode_jpeg(image_string, 1)
        image = tf.image.convert_image_dtype(image, tf.float32)

        scale = 1 / 3
        new_height, new_width = int(378 * scale), int(285 * scale)
        image = tf.image.resize_images(image, [new_height, new_width])

        return image, label

    train_dataset = train_dataset.map(parse_function, num_parallel_calls=4)
    train_dataset = train_dataset.shuffle(train_shuffle_buffer)
    train_dataset = train_dataset.batch(batch_size)

    return train_dataset.make_one_shot_iterator().get_next()

def eval_input_fn(features, labels, batch_size):

    dx_valid = tf.data.Dataset.from_tensor_slices(features)
    dy_valid = tf.data.Dataset.from_tensor_slices(labels)
    valid_dataset = tf.data.Dataset.zip((dx_valid, dy_valid))
    
    def parse_function(file_name, label):
        image_string = tf.read_file(file_name)
        image = tf.image.decode_jpeg(image_string, 1)
        image = tf.image.convert_image_dtype(image, tf.float32)

        scale = 1 / 3
        new_height, new_width = int(378 * scale), int(285 * scale)
        image = tf.image.resize_images(image, [new_height, new_width])

        return image, label

    valid_dataset = valid_dataset.map(parse_function, num_parallel_calls=4)
    valid_dataset = valid_dataset.batch(batch_size)

    return valid_dataset.make_one_shot_iterator().get_next()

smile_classifier = tf.estimator.Estimator(model_fn=build_model, model_dir='models/final_model')
tensors_to_log = {'probabilities': 'softmax'}

# split train/valid/test to 60%/20%/20%
images, labels = get_data_set()
train_images, test_images, train_labels, test_labels = train_test_split(np.asarray(images), np.asarray(labels), test_size=0.2, stratify=np.asarray(labels))
train_images, valid_images, train_labels, valid_labels = train_test_split(train_images, train_labels, test_size=0.2, stratify=train_labels)

train_images = train_images.tolist()
train_labels = train_labels.tolist()
valid_images = valid_images.tolist()
valid_labels = valid_labels.tolist()
test_images = test_images.tolist()
test_labels = test_labels.tolist()

# input for training
cust_train_input_fn = lambda: train_input_fn(
    features=train_images,
    labels=train_labels,
    batch_size=batch_size
)

# input for trained examples
cust_train2_input_fn = lambda: eval_input_fn(
    features=train_images,
    labels=train_labels,
    batch_size=batch_size
)

# input for validation
cust_eval_input_fn = lambda: eval_input_fn(
    features=valid_images,
    labels=valid_labels,
    batch_size=batch_size
)

# input for testing
cust_test_input_fn = lambda: eval_input_fn(
    features=test_images,
    labels=test_labels,
    batch_size=batch_size
)

train_spec = tf.estimator.TrainSpec(input_fn=cust_train_input_fn, max_steps=epochs)
eval_spec = tf.estimator.EvalSpec(input_fn=cust_eval_input_fn)
tf.estimator.train_and_evaluate(smile_classifier, train_spec, eval_spec)


predict_test_results = smile_classifier.predict(input_fn=cust_test_input_fn)
predict_train_results = smile_classifier.predict(input_fn=cust_train2_input_fn)

template = '\nPrediction for {} is \'{}\' ({:.1f}%), expected \'{}\''

all_test = 0
no_test_correct = 0
for idx, (pred_dict, expec) in enumerate(zip(predict_test_results, test_labels)):
    class_id = pred_dict['classes'][0]
    probability = pred_dict['probabilities'][class_id]
    if classes[class_id] == classes[expec]:
        no_test_correct += 1
    print(template.format(idx, classes[class_id], 100 * probability, classes[expec]))
    all_test += 1

all_train = 0
no_train_correct = 0
for idx, (pred_dict, expec) in enumerate(zip(predict_train_results, train_labels)):
    class_id = pred_dict['classes'][0]
    probability = pred_dict['probabilities'][class_id]
    if classes[class_id] == classes[expec]:
        no_train_correct += 1
    print(template.format(idx, classes[class_id], 100 * probability, classes[expec]))
    all_train += 1

print('\nAccuracy on training set: %.2f%%' % (no_train_correct / all_train * 100))
print('\nAccuracy on test set: %.2f%%' % (no_test_correct / all_test * 100))