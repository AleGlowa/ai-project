import math


def manhattan_distance(start_node, end_node):
    return math.sqrt(
        math.pow(abs((start_node.x - end_node.x)), 2) +
        math.pow(abs((start_node.y - end_node.y)), 2)
    )