from priorityQueue import PriorityQueue
from pizzaGuy import PizzaGuy
from graph import Graph


# enter the graph size here
graph_size = 120

# start point x and y
start_x = 0
start_y = 0

# order place x and y
order_place_x = 7
order_place_y = 7

matrix = Graph(graph_size)

# Insert the pizzeria and end place coordinates
pizzeria = matrix.table[0][0]
order_place = matrix.table[order_place_x][order_place_y]

pizzeria.place_pizzeria(order_place)

print(matrix)

# if you want to test quickly do our algorithm work just uncomment those 2 lines

# matrix.table[0][1].make_wall()
# matrix.table[1][0].make_wall()

mr_pizza = PizzaGuy(matrix, pizzeria, order_place)
closed_path = mr_pizza.start()
print('closed_path', closed_path)
