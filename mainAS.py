from priorityQueue import PriorityQueue
from pizzaGuy import PizzaGuy
from graph import Graph

def pizzaGuyFindPath(start_x,start_y,end_x,end_y):
        closedList = []
        current_node = None
        updated = False


# enter the graph size here
        graph_size_x = 11
        graph_size_y = 11

        matrix = Graph(graph_size_x,graph_size_y)

# Insert the pizzeria and end place coordinates
        pizzeria = matrix.table[start_x][start_y]
        order_place = matrix.table[end_x][end_y]

        pizzeria.place_pizzeria(order_place)

        print(matrix)
        matrix.table[9][8].make_wall()
        matrix.table[8][8].make_wall()
        matrix.table[4][2].make_wall()
        matrix.table[4][1].make_wall()
        matrix.table[5][1].make_wall()
        matrix.table[5][2].make_wall()
        matrix.table[6][1].make_wall()
        matrix.table[6][2].make_wall()
        matrix.table[5][5].make_wall()
        matrix.table[5][4].make_wall()
        matrix.table[4][4].make_wall()
        matrix.table[6][4].make_wall()
        matrix.table[4][5].make_wall()

        # my walls
        
        # 4th house
        matrix.table[0][1].make_wall()
        matrix.table[1][1].make_wall()
        matrix.table[2][1].make_wall()
        matrix.table[0][2].make_wall()
        matrix.table[1][2].make_wall()
        matrix.table[2][2].make_wall()
        
        # 5th house
        matrix.table[4][1].make_wall()
        matrix.table[4][2].make_wall()
        matrix.table[5][1].make_wall()
        matrix.table[5][2].make_wall()
        matrix.table[6][1].make_wall()
        matrix.table[6][2].make_wall()
        
        # 6th house
        matrix.table[8][1].make_wall()
        matrix.table[9][1].make_wall()
        matrix.table[10][1].make_wall()
        matrix.table[8][2].make_wall()
        matrix.table[9][2].make_wall()
        matrix.table[10][2].make_wall()
        
        # 7th house
        matrix.table[0][4].make_wall()
        matrix.table[1][4].make_wall()
        matrix.table[2][4].make_wall()
        matrix.table[0][5].make_wall()
        matrix.table[1][5].make_wall()
        matrix.table[2][5].make_wall()

        # 8th house
        matrix.table[4][4].make_wall()
        matrix.table[5][4].make_wall()
        matrix.table[6][4].make_wall()
        matrix.table[4][5].make_wall()
        matrix.table[5][5].make_wall()
        matrix.table[6][5].make_wall()
        
        # 9th house
        matrix.table[8][4].make_wall()
        matrix.table[9][4].make_wall()
        matrix.table[10][4].make_wall()
        matrix.table[8][5].make_wall()
        matrix.table[9][5].make_wall()
        matrix.table[10][5].make_wall()

        # 10th house
        matrix.table[0][7].make_wall()
        matrix.table[1][7].make_wall()
        matrix.table[2][7].make_wall()
        matrix.table[0][8].make_wall()
        matrix.table[1][8].make_wall()
        matrix.table[2][8].make_wall()

        # 11th house
        matrix.table[4][7].make_wall()
        matrix.table[5][7].make_wall()
        matrix.table[6][7].make_wall()
        matrix.table[4][8].make_wall()
        matrix.table[5][8].make_wall()
        matrix.table[6][8].make_wall()

        # 12th house
        matrix.table[8][7].make_wall()
        matrix.table[9][7].make_wall()
        matrix.table[10][7].make_wall()
        matrix.table[8][8].make_wall()
        matrix.table[9][8].make_wall()
        matrix.table[10][8].make_wall()

# if you want to test quickly do our algorithm work just uncomment those 2 lines

       
        tab=[]
        tab_ruch=[]
        mr_pizza = PizzaGuy(matrix, pizzeria, order_place,tab)
        tab=mr_pizza.ruch_self()
        for i in tab:
                if(i==1):
                        tab_ruch.append('prawo')
                elif(i==0):
                        tab_ruch.append('dol')
                elif(i==2):
                        tab_ruch.append('gora')
                elif(i==3):
                        tab_ruch.append('lewo')
        return tab_ruch