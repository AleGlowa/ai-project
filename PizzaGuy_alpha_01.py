import pygame
import time
from predict_smile_recognition import recognize_smile

dir_examples = 'new_examples/'

pygame.init()
screen = pygame.display.set_mode((600,600))
screen.fill((34,204,2))
ruch=['prawo','prawo','prawo','dol','dol','dol','prawo', 'prawo']

# dict of houses and list of directories to face images
houses = {}
url_faces = [dir_examples + 'person/person1.jpg', dir_examples + 'person/person2.jpg', dir_examples + 'person/person3.jpg', dir_examples + 'person/person4.jpg',
        dir_examples + 'person/person5.jpg', dir_examples + 'Obama/obama.jpg', dir_examples + 'waitress/waitress.jpg', dir_examples + 'JakubPiekarz/JakubPiekarz.jpg',
        dir_examples + 'JakubKsiadz/JakubKsiadz.jpg', dir_examples + 'person/person6.jpg', dir_examples + 'person/person7.jpg', dir_examples + 'person/person8.jpg']

# dictionary for counting occuriences of smile intensifies
smiles_summary = {}

class pizzaGuyVisual:
        x=0
        y=0
        def __init__(self,car_image_original,car_image_up,car_image_down,car_image_left,car_image_right,x,y ):
               self.car_image_original=car_image_original
               self.car_image_up=car_image_up
               self.car_image_down=car_image_down
               self.car_image_left=car_image_left
               self.car_image_right=car_image_right
               self.x=x
               self.y=y
        def move_x_y(self,movement,max_y_x_val):
                if(movement=="lewo"):
                        self.car_image_original=self.car_image_left
                        if(self.x<max_y_x_val):
                                self.x-=40
                                
                        else:
                                self.x=max_y_x_val
                elif(movement=="prawo"):
                             self.car_image_original=self.car_image_right
                             if(self.x<max_y_x_val):
                                self.x+=40
                             else:
                                self.x=max_y_x_val
                elif(movement=="gora"):
                           self.car_image_original=self.car_image_up
                           if(self.y<max_y_x_val):
                                self.y-=40
                           else:
                                self.y=max_y_x_val
                elif(movement=="dol"):
                           self.car_image_original=self.car_image_down
                           if(self.y<max_y_x_val):
                                self.y+=40
                           else:
                                self.y=max_y_x_val
        def printxy(self,screen):
                screen.blit(self.car_image_original,(self.x,self.y))

# House class
class House:
        def __init__(self, url_face, person_name, pos):
                self.url_face = url_face
                self.person_name = person_name
                self.pos_x, self.pos_y = pos

# update occurences of smiles
def update_no_smiles(result, no_smiles):
        if result == 'high intensity smile':
                no_smiles[0] += 1
                smiles_summary[result] = no_smiles[0]
        elif result == 'low intensity smile':
                no_smiles[1] += 1
                smiles_summary[result] = no_smiles[1]
        elif result == 'no smile':
                no_smiles[2] += 1
                smiles_summary[result] = no_smiles[2]

def createHouses(myfont):
        house_x=[]
        house_y=[]
        house_number=0
        road_image=pygame.image.load('images/road_grid.png')
        image_house=pygame.image.load('images/house.png')
        image_house_big=pygame.transform.scale(image_house,(80,80))
        for k in range(40,520,40):
                for l in range(160,560,40):
                        screen.blit(road_image,(k,l))
                        
        for i in range(80,480,120):
                for j in range(60,480,160):
                        screen.blit(image_house_big,(j,i))
                        house_x.append(j+20)
                        house_y.append(i+80)
                        print(house_x[house_number],house_y[house_number],house_number+1)
                        label = myfont.render((""+str(house_number+1)), 1, (0,0,0))
                        screen.blit(label, (j+30, i+35))
                        house_number=house_number+1
                        
                        # add houses objects
                        house = House(url_faces[house_number - 1], url_faces[house_number - 1].split('/')[-2], (house_x[-1], house_y[-1]))
                        houses[house_number] = house
                        
def load_scene():
        
        myfont = pygame.font.SysFont("bold", 35)
        logo=pygame.image.load('images/pizza_slice.png')
        pygame.display.set_icon(logo)
        pygame.display.set_caption('PIZZA GUY')
        print('Wczytywanie i skalowanie tekstur...')
        createHouses(myfont)
            
        
def main():
        run_x=0
        run_y=0
        no_smiles = [0, 0, 0]
        car_to_create=pygame.image.load('images/pizza_guy2.png')
        car_to_create_rescale=pygame.transform.scale(car_to_create,(40,40))
        car_to_create_left=pygame.transform.rotate(car_to_create_rescale,90)
        car_to_create_right=pygame.transform.rotate(car_to_create_rescale,-90)
        car_to_create_down=pygame.transform.rotate(car_to_create_rescale,180)
        pizzaGuyRun=pizzaGuyVisual(car_to_create_rescale,car_to_create_rescale,car_to_create_down,car_to_create_left,car_to_create_right,40,160)
        run=True
        load_scene()
        # set destination house
        destination = houses[5]
        go_to_location=0
        while(run):

                if(go_to_location==0):
                        for i in range(0,len(ruch)):
                                pygame.time.delay(280)
                                pizzaGuyRun.move_x_y(ruch[i],450)
                                pizzaGuyRun.printxy(screen)
                                pygame.display.update()
                        go_to_location=1

                        # predict smile or not and update dictionary of occurences of smiles
                        result = recognize_smile(destination.url_face, destination.person_name)
                        update_no_smiles(result, no_smiles)
                
                for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                                run = False
main()